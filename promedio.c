#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//LA funcion final solicitada
void reemplazar(char linea[], char viejo, char nuevo)
{
    int i;
    for (i = 0; i < 100; ++i)
    {
        if (linea[i] == viejo)
        {
            linea[i] = nuevo;
            break;
        }
    }
}
static int TAMANO_MAX = 100;
//este es el menu principal
int main(int argc, char *argv[])
{

    int numeros[TAMANO_MAX];
    char linea[TAMANO_MAX];
    int cantidad;
    float promedio;
    float suma = 0.0;
    for (int j = 0; j < TAMANO_MAX; ++j)
    {
        numeros[j] = 0;
    }
    //primero se verifica que no sobrepase lo definido en el documento 
    do
    {
        printf("Este programa calcula el promedio de los n�meros\n");
        printf(" Cuantos numeros va a ingresar? \n");
        fgets(linea, 100, stdin);
        reemplazar(linea, '\n', '\0');
        cantidad = atoi(linea);
        if (cantidad > 100)
        {
            printf("Cantidad fuera del rango establecido \n");
            printf("Ingresa una nueva cantidad \n");
        }

        break;
    } while (cantidad > 100);
    //En esta parte se procede establecer los valores que seran colocados 
    for (int i = 0; i < cantidad; ++i)
    {
        printf("  Numero %d: ", i + 1);
        fgets(linea, 100, stdin);
        reemplazar(linea, '\n', '\0');
        numeros[i] = atoi(linea);

        suma += numeros[i];
    }
    promedio = suma / (float)cantidad;
    printf("El promedio de los numeros es %.1f\n", promedio);
    return 0;
}

